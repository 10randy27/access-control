package ch.schneider.fxapp;

import ch.schneider.fxapp.dependency_injection.config.ModuleConfig;
import ch.schneider.fxapp.dependency_injection.service.FxmlLoaderService;
import ch.schneider.fxapp.events.StageEvent;
import com.google.common.eventbus.EventBus;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by grazi on 20.08.16.
 */
public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
    primaryStage.setTitle("Access Control Anwendung");

        final Injector injector = Guice.createInjector(new ModuleConfig());
        FxmlLoaderService fxmlLoaderService = injector.getInstance(FxmlLoaderService.class);

        AnchorPane rootPane = showRootView(fxmlLoaderService, primaryStage);

        List<AnchorPane> anchorPanes = new ArrayList<>();
        anchorPanes.add(loadAnchorPaneXML(fxmlLoaderService, "PersonView.fxml"));
        anchorPanes.add(loadAnchorPaneXML(fxmlLoaderService, "KarteView.fxml"));
        anchorPanes.add(loadAnchorPaneXML(fxmlLoaderService, "TuereView.fxml"));
        anchorPanes.add(loadAnchorPaneXML(fxmlLoaderService, "PlanView.fxml"));

        setPanesinTabs(rootPane, anchorPanes);


        final EventBus eventBus = injector.getInstance(EventBus.class);
        eventBus.post(new StageEvent(primaryStage));
    }

    private AnchorPane loadAnchorPaneXML (FxmlLoaderService fxmlLoaderService, String xmlName) {

        return (AnchorPane) fxmlLoaderService.load("view/" + xmlName);

    }

    private void setPanesinTabs (AnchorPane rootPane, List<AnchorPane> panes) {
        TabPane tabPane = (TabPane) rootPane.getChildren().get(0);
        ObservableList<Tab> tabs = tabPane.getTabs();

        for (int i = 0; i < tabs.size(); i++) {
            tabs.get(i).setContent(panes.get(i));
        }
    }

    /**
     * Shows the person overview inside the root layout.
     */
    private AnchorPane showRootView(FxmlLoaderService fxmlLoaderService, Stage primaryStage) {

        AnchorPane rootView = (AnchorPane) fxmlLoaderService.load("view/RootView.fxml");

        // Show the scene containing the root layout.
        Scene scene = new Scene(rootView);
        primaryStage.setScene(scene);
        primaryStage.show();

        return rootView;
    }



    public static void main(String[] args) {
        launch(args);
    }

}
