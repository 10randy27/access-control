package ch.schneider.fxapp.dao.inMemory;


import ch.schneider.fxapp.dao.PersonDAO;
import ch.schneider.fxapp.model.Person;
import ch.schneider.fxapp.util.IdGenerator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.LocalDate;

/**
 * Created by grazi on 29.05.16.
 */
@Singleton
public class PersonDAOInMemory implements PersonDAO {

    /**
     * The data as an observable list of Persons.
     */
    private ObservableList<Person> personData = FXCollections.observableArrayList();

    IdGenerator idGen;

    @Inject
    public PersonDAOInMemory(IdGenerator idGen) {
        this.idGen = idGen;
        loadPersons(null);
    }


    /**
     * Returns the data as an observable list of Persons.
     * @return
     */
    @Override
    public ObservableList<Person> getAllPersons() {
        return personData;
    }

    @Override
    public void loadPersons(String location) {
        // Add some sample data
        Person hans = new Person();
        hans.setVorname("Hans");
        hans.setNachname("Lustig");
        hans.setGeburtstag(LocalDate.of(1981, 02, 18));
        hans.setPersonalNummer("abc-123");
        hans.setKarteId(123);
        hans.setZutrittPlanId(123);
        hans.setGueltVon(LocalDate.of(2016, 8, 21));
        hans.setGueltVon(LocalDate.of(2020, 12, 31));
        addPerson(hans);

    }

    @Override
    public void addPerson(Person person) {
        person.setId(idGen.getNewId(person.getId()));
        personData.add(person);
    }

    @Override
    public void updatePerson(Person person) {
        // doNothing
    }

    @Override
    public void deletePerson(Person person) {
        personData.remove(person);
    }
}
