package ch.schneider.fxapp.dao;


import ch.schneider.fxapp.model.Person;
import javafx.collections.ObservableList;

/**
 * Created by grazi on 14.06.16.
 */
public interface PersonDAO {

    /**
     *  Returns the data as an observable list of Persons.
     *
     * @return ObservableList with all the Persons from Datasource
     */
    public ObservableList<Person> getAllPersons();

    /**
     *  Load the data from a DataSource
     *
     */
    public void loadPersons(String location);

    public void addPerson(Person person);

    public void updatePerson(Person person);

    public void deletePerson(Person person);

}
