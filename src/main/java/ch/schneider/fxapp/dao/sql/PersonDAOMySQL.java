package ch.schneider.fxapp.dao.sql;

import ch.schneider.fxapp.dao.PersonDAO;
import ch.schneider.fxapp.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.time.LocalDate;

/**
 * Created by grazi on 19.06.16.
 */
public class PersonDAOMySQL implements PersonDAO {

    private static final String ALL_PERSONS_SQL = "SELECT id, firstname, lastname, street, postalcode, city, birthday from person ";
    private static final String DELETE_ALL_PERSONS_SQL = "DELETE FROM person ";
    private static final String DELETE_PERSON_SQL = "DELETE FROM person WHERE id = ";


    private Connection connection;

    private ObservableList<Person> personData = FXCollections.observableArrayList();


    @Override
    public ObservableList<Person> getAllPersons() {
        return personData;
    }

    @Override
    public void loadPersons(String location) {
        try {
            connection = connectToDb(location);
            createTableIfNotExists(connection);
            loadPersonsFromDb(connection);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    private void loadPersonsFromDb(Connection connection) throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet resultSet = stmt.executeQuery(ALL_PERSONS_SQL);
        while (resultSet.next()) {
            Person p = new Person();
//            p.setPersonId(resultSet.getInt("id"));
//            p.setFirstName(resultSet.getString("firstname"));
//            p.setLastName(resultSet.getString("lastname"));
//            p.setStreet(resultSet.getString("street"));
//            p.setPostalCode(resultSet.getInt("postalcode"));
//            p.setCity(resultSet.getString("city"));
//            p.setBirthday(LocalDate.parse(resultSet.getString("birthday")));
            personData.add(p);
        }

    }

    private Connection connectToDb(String location) throws SQLException {

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url = "jdbc:sqlite:" + location;
        Connection conn = DriverManager.getConnection(url);
        if (conn == null) {
            throw new IllegalStateException("DB Connection not established!!");
        }
        return conn;
    }

    private void createTableIfNotExists(Connection connection) throws SQLException {

        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS person (\n"
                + "	id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "	firstname text NOT NULL,\n"
                + "	lastname text NOT NULL,\n"
                + "	street text NOT NULL,\n"
                + "	postalcode integer NOT NULL,\n"
                + "	city text NOT NULL,\n"
                + "	birthday text NOT NULL\n"
                + ");";

        Statement stmt = connection.createStatement();
        // create a new table
        stmt.execute(sql);
    }

    @Override
    public void addPerson(Person person) {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(new InsertSQLBuilder()
//                                .addValue("'" + person.getFirstName() + "'")
//                                .addValue("'" + person.getLastName() + "'")
//                                .addValue("'" + person.getStreet() + "'")
//                                .addValue(String.valueOf(person.getPostalCode()))
//                                .addValue("'" + person.getCity() + "'")
//                                .addValue("'" + String.valueOf(person.getBirthday()) + "'")
                                .getInsertSQL()
                             );
            personData.clear();
            loadPersonsFromDb(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePerson(Person person) {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(new UpdateSQLBuilder()
//                    .addValue("firstname", "'" + person.getFirstName() + "'")
//                    .addValue("lastname", "'" + person.getLastName() + "'")
//                    .addValue("street", "'" + person.getStreet() + "'")
//                    .addValue("postalcode", String.valueOf(person.getPostalCode()))
//                    .addValue("city", "'" + person.getCity() + "'")
//                    .addValue("birthday", "'" + String.valueOf(person.getBirthday()) + "'")
//                    .addWhere(person.getPersonId())
                    .getInsertSQL()
            );
            personData.clear();
            loadPersonsFromDb(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deletePerson(Person person) {
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(DELETE_PERSON_SQL + person.getId());
            personData.clear();
            loadPersonsFromDb(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private final static class InsertSQLBuilder {

        public final static String INSERT_SQL = "INSERT INTO person (firstname, lastname, street, postalcode, city, birthday) VALUES (";

        private int valueCounter = 0;

        private StringBuilder sb = new StringBuilder();

        public InsertSQLBuilder() {
            sb.append(INSERT_SQL);
        }

        public InsertSQLBuilder addValue (String value) {
            if (valueCounter != 0) {
                sb.append(",");
            }
            sb.append(value);
            valueCounter++;
            return this;
        }

        public String getInsertSQL () {
            return sb.toString() + ") ";
        }

    }

    private final static class UpdateSQLBuilder {

        public final static String UPDATE_SQL = "UPDATE person SET ";

        private int valueCounter = 0;

        private StringBuilder sb = new StringBuilder();

        public UpdateSQLBuilder() {
            sb.append(UPDATE_SQL);
        }

        public UpdateSQLBuilder addValue (String attr, String value) {
            if (valueCounter != 0) {
                sb.append(",");
            }
            sb.append(attr);
            sb.append(" = ");
            sb.append(value);
            valueCounter++;
            return this;
        }

        public UpdateSQLBuilder addWhere (int whereValue) {
            sb.append(" WHERE id = " + whereValue);
            return this;
        }

        public String getInsertSQL () {
            return sb.toString() + ";";

        }

    }

}
