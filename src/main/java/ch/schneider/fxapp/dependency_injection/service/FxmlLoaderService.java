package ch.schneider.fxapp.dependency_injection.service;

import javafx.scene.Parent;

public interface FxmlLoaderService {

    Parent load(String path);
}
