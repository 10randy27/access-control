package ch.schneider.fxapp.dependency_injection.config;

import ch.schneider.fxapp.MainApp;
import ch.schneider.fxapp.dao.PersonDAO;
import ch.schneider.fxapp.dao.inMemory.PersonDAOInMemory;
import ch.schneider.fxapp.dependency_injection.service.FxmlLoaderService;
import ch.schneider.fxapp.dependency_injection.service.impl.FxmlLoaderServiceImpl;
import ch.schneider.fxapp.util.IdGenerator;
import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;

public final class ModuleConfig extends AbstractModule {
    @Override
    protected void configure() {
        bind(EventBus.class).asEagerSingleton();
        bind(IdGenerator.class).asEagerSingleton();
        bind(PersonDAO.class).to(PersonDAOInMemory.class).asEagerSingleton();
        bind(FxmlLoaderService.class).to(FxmlLoaderServiceImpl.class).asEagerSingleton();
        bind(MainApp.class);
    }
}
