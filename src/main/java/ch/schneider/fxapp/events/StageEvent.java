package ch.schneider.fxapp.events;

import javafx.stage.Stage;

/**
 * Created by grazi on 20.08.16.
 */
public class StageEvent {

    private Stage stage;

    public StageEvent (Stage stage) {
        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }
}
