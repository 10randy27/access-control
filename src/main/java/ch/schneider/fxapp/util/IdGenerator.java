package ch.schneider.fxapp.util;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by grazi on 19.06.16.
 */
public class IdGenerator {

    private static AtomicInteger idCounter = new AtomicInteger();
    private static Set<Integer> ids = new HashSet<>();

    public int getNewId(int id) {
        int newId = id;
        if (newId < 0) {
            newId = idCounter.incrementAndGet();
        }
        if (ids.contains(newId)) {
            throw new IllegalStateException("Person mit id: " + newId + " bereits erfasst!!");
        }
        ids.add(newId);
        return newId;
    }

}
