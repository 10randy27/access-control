package ch.schneider.fxapp.model;



import javafx.beans.property.*;

import java.time.LocalDate;

/**
 * Created by grazi on 20.08.16.
 */
public class Person {

    private final IntegerProperty id;
    private final StringProperty vorname;
    private final StringProperty nachname;
    private final ObjectProperty<LocalDate> geburtstag;
    private final StringProperty personalNummer;
    private final ObjectProperty<LocalDate> gueltVon;
    private final ObjectProperty<LocalDate> gueltBis;

    private final IntegerProperty karteId;
    private final IntegerProperty zutrittPlanId;


    public Person () {
        this.id = new SimpleIntegerProperty(-1);
        this.vorname = new SimpleStringProperty("");
        this.nachname = new SimpleStringProperty("");
        this.geburtstag = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 2, 21));
        this.personalNummer = new SimpleStringProperty("");

        this.gueltVon = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 2, 21));
        this.gueltBis = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 2, 21));

        this.karteId = new SimpleIntegerProperty(-1);
        this.zutrittPlanId = new SimpleIntegerProperty(-1);

    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getVorname() {
        return vorname.get();
    }

    public StringProperty vornameProperty() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname.set(vorname);
    }

    public String getNachname() {
        return nachname.get();
    }

    public StringProperty nachnameProperty() {
        return nachname;
    }

    public void setNachname(String nachname) {
        this.nachname.set(nachname);
    }

    public LocalDate getGeburtstag() {
        return geburtstag.get();
    }

    public ObjectProperty<LocalDate> geburtstagProperty() {
        return geburtstag;
    }

    public void setGeburtstag(LocalDate geburtstag) {
        this.geburtstag.set(geburtstag);
    }

    public String getPersonalNummer() {
        return personalNummer.get();
    }

    public StringProperty personalNummerProperty() {
        return personalNummer;
    }

    public void setPersonalNummer(String personalNummer) {
        this.personalNummer.set(personalNummer);
    }

    public LocalDate getGueltVon() {
        return gueltVon.get();
    }

    public ObjectProperty<LocalDate> gueltVonProperty() {
        return gueltVon;
    }

    public void setGueltVon(LocalDate gueltVon) {
        this.gueltVon.set(gueltVon);
    }

    public LocalDate getGueltBis() {
        return gueltBis.get();
    }

    public ObjectProperty<LocalDate> gueltBisProperty() {
        return gueltBis;
    }

    public void setGueltBis(LocalDate gueltBis) {
        this.gueltBis.set(gueltBis);
    }

    public int getKarteId() {
        return karteId.get();
    }

    public IntegerProperty karteIdProperty() {
        return karteId;
    }

    public void setKarteId(int karteId) {
        this.karteId.set(karteId);
    }

    public int getZutrittPlanId() {
        return zutrittPlanId.get();
    }

    public IntegerProperty zutrittPlanIdProperty() {
        return zutrittPlanId;
    }

    public void setZutrittPlanId(int zutrittPlanId) {
        this.zutrittPlanId.set(zutrittPlanId);
    }

}
