package ch.schneider.fxapp.model;

import javafx.beans.property.IntegerProperty;

/**
 * Created by Marc on 28.08.2016.
 */
public class AccessPlanDoor {
    private final IntegerProperty accessplanid;
    private final IntegerProperty doorid;

    public AccessPlanDoor(IntegerProperty accessplanid, IntegerProperty doorid) {
        this.accessplanid = accessplanid;
        this.doorid = doorid;
    }

    public int getAccessplanid() {
        return accessplanid.get();
    }

    public IntegerProperty accessplanidProperty() {
        return accessplanid;
    }

    public int getDoorid() {
        return doorid.get();
    }

    public IntegerProperty dooridProperty() {
        return doorid;
    }

    public void setAccessplanid(int accessplanid) {
        this.accessplanid.set(accessplanid);
    }

    public void setDoorid(int doorid) {
        this.doorid.set(doorid);
    }
}
