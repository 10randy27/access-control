package ch.schneider.fxapp.model;

import javafx.beans.property.IntegerProperty;

/**
 * Created by Marc on 28.08.2016.
 */
public class IdCard{
        private final IntegerProperty id;
        private final IntegerProperty uid;

    public IdCard(IntegerProperty id, IntegerProperty uid) {
        this.id = id;
        this.uid = uid;
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public int getUid() {
        return uid.get();
    }

    public IntegerProperty uidProperty() {
        return uid;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public void setUid(int uid) {
        this.uid.set(uid);
    }
}
