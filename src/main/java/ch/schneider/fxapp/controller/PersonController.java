package ch.schneider.fxapp.controller;

import ch.schneider.fxapp.dao.PersonDAO;
import ch.schneider.fxapp.dependency_injection.service.FxmlLoaderService;
import ch.schneider.fxapp.events.StageEvent;
import ch.schneider.fxapp.model.Person;
import ch.schneider.fxapp.util.DateUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javafx.beans.property.IntegerProperty;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.inject.Inject;

/**
 * Created by grazi on 20.08.16.
 */
public class PersonController {

    @FXML
    private TableView<Person> personTable;
    @FXML
    private TableColumn<Person, Integer> idColumn;
    @FXML
    private TableColumn<Person, String> vornameColumn;
    @FXML
    private TableColumn<Person, String> nachnameColumn;

    @FXML
    TextField idField;
    @FXML
    TextField vornameField;
    @FXML
    TextField nachnameField;
    @FXML
    TextField geburtstagField;
    @FXML
    TextField personalNrField;
    @FXML
    TextField validFromField;
    @FXML
    TextField validToField;
    @FXML
    TextField karteUuidField;
    @FXML
    TextField accesPlanIdField;

    @Inject
    FxmlLoaderService fxmlLoaderService;

    @Inject
    EventBus eventBus;

    private boolean personEdited;

    private Stage primaryStage;

    @Inject
    private PersonDAO personDAO;

    public void handleNewPerson () {
        Person person = new Person();
        showPersonEditDialog(person, true);
        if (personEdited) {
            personDAO.addPerson(person);
        }

    }

    private void showPersonEditDialog(Person person, boolean isNewPerson) {
        // Load the fxml file and create a new stage for the popup dialog.
        AnchorPane page = (AnchorPane) fxmlLoaderService.load("view/PersonEditView.fxml");

        // Create the dialog Stage.
        Stage dialogStage = new Stage();
        dialogStage.setTitle("Edit Person");
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);
        Scene scene = new Scene(page);
        dialogStage.setScene(scene);

//        eventBus.post(new PersonEditEvent(dialogStage, person, isNewPerson));

        // Show the dialog and wait until the user closes it
        dialogStage.showAndWait();

    }

    @FXML
    private void initialize() {
        // Initialize the person table with the two columns.
        idColumn.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
        vornameColumn.setCellValueFactory(cellData -> cellData.getValue().vornameProperty());
        nachnameColumn.setCellValueFactory(cellData -> cellData.getValue().nachnameProperty());

        // Add observable list data to the table
        personTable.setItems(personDAO.getAllPersons());

        // Clear person details.
        showPersonDetails(null);

        // Listen for selection changes and show the person details when changed.
        personTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showPersonDetails(newValue));
    }


    /**
     * Fills all text fields to show details about the person.
     * If the specified person is null, all text fields are cleared.
     *
     * @param person the person or null
     */
    private void showPersonDetails(Person person) {
        if (person != null) {
            // Fill the labels with info from the person object.
            idField.setText(String.valueOf(person.getId()));
            vornameField.setText(person.getVorname());
            nachnameField.setText(person.getNachname());
            geburtstagField.setText(DateUtil.format(person.getGeburtstag()));
            personalNrField.setText(person.getPersonalNummer());
            validFromField.setText(DateUtil.format(person.getGueltVon()));
            validToField.setText(DateUtil.format(person.getGueltBis()));
            karteUuidField.setText(String.valueOf(person.getKarteId()));
            accesPlanIdField.setText(String.valueOf(person.getZutrittPlanId()));
        } else {
            // Person is null, remove all the text.
//            idField.setText(String.valueOf(person.getId()));
            vornameField.setText("");
            nachnameField.setText("");
            geburtstagField.setText("");
            personalNrField.setText("");
            validFromField.setText("");
            validToField.setText("");
            karteUuidField.setText("");
            accesPlanIdField.setText("");
        }
    }

    @Subscribe
    public void receiveStageEvent (StageEvent stageEvent) {
        this.primaryStage = stageEvent.getStage();
    }

}
